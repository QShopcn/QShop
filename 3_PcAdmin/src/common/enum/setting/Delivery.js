import Enum from '../enum'
/**
 * 枚举类：文件存储方式
 * DeliveryEnum
 */
export default new Enum([
    { key: 'KuaiDi100', name: '快递100', value: 'kuaiDi100' },
    { key: 'KuaiDiNiao', name: '快递鸟', value: 'kuaiDiNiao' }
])