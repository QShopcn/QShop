import * as Api from '@/api/sys/sysArea'
import storage from 'store'
import * as util from '@/utils/util'
const TreeArea = 'TreeArea'
const ListArea = 'ListArea'
    /**
     * 商品分类 model类
     * RegionModel
     */
export default {

    // 从服务端获取全部地区数据(树状)
    getDataFromApi() {
        return new Promise((resolve, reject) => {
            Api.listByWhere({ maxLevel: 2 }).then(res => {
                var list = res.result;
                resolve(list)
            })
        })
    },

    // 获取所有地区(树状)
    getTreeData() {
        return new Promise((resolve, reject) => {
            // 判断缓存中是否存在
            const data = storage.get(TreeArea)
                // 从服务端获取全部地区数据
            if (data) {
                resolve(data)
            } else {
                this.getDataFromApi().then(list => {
                    var treeData = this.formatRegionTree(list)
                        // 缓存24小时
                    storage.set(ListArea, list, 24 * 60 * 60)
                    storage.set(TreeArea, list, 24 * 60 * 60)
                    resolve(treeData)
                })
            }
        })
    },
    /**
     * 格式化树结构数据
     * @param {Object} result
     */
    formatRegionTree(list) {
        var arr = list.map(function(item) {
            return {
                id: item.id,
                parentId: item.parentId == '0' ? null : item.parentId,
                label: item.name,
                value: item.id,
                level: item.level
            };
        });

        var listTree = util.listToTree(arr)

        // console.log("listTree", listTree)
        return listTree
    },
    // 获取所有地区的总数
    getCitysCount() {
        return new Promise((resolve, reject) => {
            // 获取所有地区(树状)
            this.getTreeData().then(list => {
                const listData = storage.get(ListArea)
                var arrCity = listData.filter((item) => item.level == 2);
                console.log("list3333", arrCity.length)
                resolve(arrCity.length)
            })
        })
    }

}