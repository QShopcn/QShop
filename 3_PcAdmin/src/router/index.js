import Vue from 'vue'
import Router from 'vue-router'

// in development-env not use lazy-loading, because lazy-loading too many pages will cause webpack hot update too slow. so only in production use lazy-loading;
// detail: https://panjiachen.github.io/vue-element-admin-site/#/lazy-loading

Vue.use(Router)

/* Layout */
import Layout from '../views/layout/Layout'

/**
* hidden: true                   if `hidden:true` will not show in the sidebar(default is false)
* alwaysShow: true               if set true, will always show the root menu, whatever its child routes length
*                                if not set alwaysShow, only more than one route under the children
*                                it will becomes nested mode, otherwise not show the root menu
* redirect: noredirect           if `redirect:noredirect` will no redirct in the breadcrumb
* name:'router-name'             the name is used by <keep-alive> (must set!!!)
* meta : {
    title: 'title'               the name show in submenu and breadcrumb (recommend set)
    icon: 'svg-name'             the icon show in the sidebar,
  }
**/
export const constantRouterMap = [{
        path: '/install',
        component: () =>
            import ('@/views/sys/install/index'),
        meta: {
            title: '安装管理后端',
            content: {
                keywords: 'QShop商城安装管理后端',
                description: 'QShop商城安装管理后端',
            },
        },
        hidden: true
    }, {
        path: '/login',
        component: () =>
            import ('@/views/sys/login/index'),
        meta: {
            title: '登录',
            content: {
                keywords: 'QShop登录页',
                description: 'QShop登录页',
            },
        },
        hidden: true
    },
    {
        path: '/404',
        component: () =>
            import ('@/views/sys/errorPage/404'),
        meta: {
            sortNo: 0
        },
        hidden: true
    },
    {
        path: '/401',
        component: () =>
            import ('@/views/sys/errorPage/401'),
        meta: {
            sortNo: 0
        },
        hidden: true
    },
    {
        path: '/',
        component: Layout,
        redirect: 'dashboard',
        name: 'layout',
        meta: {
            sortNo: 0
        },
        children: [{
                path: '/dashboard',
                name: 'dashboard',
                meta: {
                    title: '主页',
                    icon: 'iconfont icon-zhuyeicon',
                    sortNo: 0
                },
                component: () =>
                    import ('@/views/sys/dashboard/index')
            }, {
                path: '/profile',
                name: 'profile',
                hidden: true,
                meta: {
                    title: '个人中心',
                    icon: 'guide',
                    sortNo: 0
                },
                component: () =>
                    import ('@/views/user/adminuser/profile')
            },

            // {
            // // path: '/iframePage/:url/:name',
            // path: '/iframePage/:code',
            // name: 'iframePage',
            // hidden: true,
            // meta: {
            //     title: '接口文档',
            //     icon: 'guide',
            //     sortNo: 0
            // },
            // component: () =>
            //     import ('@/views/iframePage/index')
            // }
        ]
    },
    // {
    //   path: '/',
    //   hidden: true,
    //   component: Layout,
    //   meta: { sortNo: 0 },
    //   children: [{
    //     path: '/profile',
    //     name: 'profile',
    //     meta: { title: '个人中心', icon: 'guide', sortNo: 0 },
    //     component: () => import('@/views/user/adminuser/profile')
    //   }]
    // }
    // {
    //   path: '/swagger',
    //   component: Layout,
    //   meta: { sortNo: 0 },
    //   children: [{
    //     path: '',
    //     name: 'swagger',
    //     meta: { title: '接口文档', icon: 'guide', sortNo: 0 },
    //     component: () => import('@/views/swagger/index')
    //   }]
    // }

    //商品编辑路由
    {
        path: '/goods',
        component: Layout,
        redirect: 'noredirect',
        name: 'goods',
        meta: {
            title: '商品',
            icon: 'eye',
        },
        children: [{
            path: 'edit/:id?',
            component: () =>
                import ('@/views/goods/goods/edit'),
            name: 'goodsEdit',
            meta: {
                title: '新增/编辑商品',
            },
        }, ],
    },
    //订单详情路由
    {
        path: '/order',
        component: Layout,
        redirect: 'noredirect',
        name: 'order',
        meta: {
            title: '订单',
        },
        children: [{
                path: 'details/:id',
                component: () =>
                    import ('@/views/order/order/details'),
                name: 'orderDetail',
                meta: {
                    title: '订单详情',
                },
            },
            {
                path: "refundDetails",
                component: () =>
                    import ("@/views/order/order/refundDetails"),
                name: "refundDetails",
                hidden: true,
                meta: {
                    notauth: true,
                    title: "售后订单详情",
                    noCache: true,
                    icon: "list",
                },
            },
        ],
    },
]

var router = new Router({
    // mode: 'history', //后端支持可开
    scrollBehavior: () => ({
        y: 0
    }),
    routes: constantRouterMap
})

export default router