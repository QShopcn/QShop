﻿import request from '@/utils/request'

export function load(params) {
  return request({
    url: '/storeWxShipping/load',
    method: 'get',
    params
  })
}
export function listByWhere(params) {
  return request({
    url: '/storeWxShipping/listByWhere',
    method: 'get',
    params
  })
}
export function getDetail(params) {
  return request({
    url: '/storeWxShipping/get',
    method: 'get',
    params
  })
}
export function addOrUpdate(data) {
  return request({
    url: '/storeWxShipping/addOrUpdate',
    method: 'post',
    data
  })
}
export function del(data) {
  return request({
    url: '/storeWxShipping/delete',
    method: 'post',
    data
  })
}

