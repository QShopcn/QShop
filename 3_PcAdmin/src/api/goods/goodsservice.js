﻿import request from '@/utils/request'

export function load(params) {
    return request({
        url: '/goodsService/load',
        method: 'get',
        params
    })
}
export function listByWhere(params) {
    return request({
        url: '/goodsService/listByWhere',
        method: 'get',
        params
    })
}
export function getDetail(params) {
    return request({
        url: '/goodsService/get',
        method: 'get',
        params
    })
}
export function addOrUpdate(data) {
    return request({
        url: '/goodsService/addOrUpdate',
        method: 'post',
        data
    })
}
export function del(data) {
    return request({
        url: '/goodsService/delete',
        method: 'post',
        data
    })
}