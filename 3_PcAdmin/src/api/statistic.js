import request from '@/utils/request'

export function loadFreight(params) {
    return request({
        url: '/statistic/loadFreight',
        method: 'get',
        params
    })
}
//订单首页
export function loadPcHomePageData(params) {
    return request({
        url: '/statistic/loadPcHomePageData',
        method: 'get',
        params
    })
}
//统计数据
export function loadStatisticData(params) {
    return request({
        url: '/statistic/loadStatisticData',
        method: 'get',
        params
    })
}
//统计数据图标
export function loadStatisticEchartTable(params) {
    return request({
        url: '/statistic/loadStatisticEchartTable',
        method: 'get',
        params
    })
}

export function loadPartner(params) {
    return request({
        url: '/statistic/loadPartner',
        method: 'get',
        params
    })
}
export function ListLinqUserIncomeByUser(params) {
    return request({
        url: '/statistic/ListLinqUserIncomeByUser',
        method: 'get',
        params
    })
}

// 经营统计-代理经销商数据
export function StatisticOperate(params) {
    return request({
        url: '/statistic/ListLinqUserIncomeByUser',
        method: 'get',
        params
    })
}