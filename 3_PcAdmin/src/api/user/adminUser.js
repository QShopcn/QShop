import request from '@/utils/request'

export function getList(params) {
    return request({
        url: '/adminUser/load',
        method: 'get',
        params
    })
}

export function load(params) {
    return request({
        url: '/adminUser/load',
        method: 'get',
        params
    })
}

export function loadAll(params) {
    return request({
        url: '/adminUser/loadall',
        method: 'get',
        params
    })
}

export function get(params) {
    return request({
        url: '/adminUser/get',
        method: 'get',
        params
    })
}
export function addOrUpdateEmp(data) {
    return request({
        url: '/adminUser/AddOrUpdateEmp',
        method: 'post',
        data
    })
}
export function changePassword(data) {
    return request({
        url: '/adminUser/changepassword',
        method: 'post',
        data
    })
}
export function changeProfile(data) {
    return request({
        url: '/adminUser/changeprofile',
        method: 'post',
        data
    })
}
export function changeUserType(data) {
    return request({
        url: '/adminUser/ChangeUserType',
        method: 'post',
        data
    })
}

export function del(data) {
    return request({
        url: '/adminUser/delete',
        method: 'post',
        data
    })
}

export function loadByRole(params) {
    return request({
        url: '/adminUser/loadByRole',
        method: 'get',
        params
    })
}
export function LoadByOrg(params) {
    return request({
        url: '/adminUser/LoadByOrg',
        method: 'get',
        params
    })
}