import request from '@/utils/request'

export function getList(params) {
    return request({
        url: '/user/load',
        method: 'get',
        params
    })
}
export function load(params) {
    return request({
        url: '/user/load',
        method: 'get',
        params
    })
}
export function get(params) {
    return request({
        url: '/user/get',
        method: 'get',
        params
    })
}
export function changeUserType(data) {
    return request({
        url: '/user/ChangeUserType',
        method: 'post',
        data
    })
}
export function rechargeBalance(data) {
    return request({
        url: '/user/RechargeBalance',
        method: 'post',
        data
    })
}


export function del(data) {
    return request({
        url: '/user/delete',
        method: 'post',
        data
    })
}