import config from '@/config.js'
import storage from '@/utils/storage'

// 合并用户配置和默认配置
const mergeConfig = Object.assign({}, config)

/**
 * 配置文件工具类
 * mix: 如需在项目中获取配置项, 请使用本工具类的方法, 不要直接import根目录的config.js
 */
export default {
	// 获取全部配置
	all() {
		return mergeConfig
	},

	// 获取指定配置
	get(key, def = undefined) {
		if (mergeConfig.hasOwnProperty(key)) {
			return mergeConfig[key]
		}
		console.error(`检测到不存在的配置项: ${key}`)
		return def
	},
	getWxAppId() {
		var appId = "";
		// #ifdef MP-WEIXIN
		appId = uni.getAccountInfoSync().miniProgram.appId;
		// #endif
		return appId;
	},
	getH5StoreId() {
		var storeId = "";
		// #ifdef H5
		var url = window.location.href;
		storeId = this.queryURLParams(url, "storeId");
		if (storeId == "") {
			storeId = storage.get("storeId");
		} else {
			// 保存storeId到缓存(3天)
			storage.set("storeId", storeId, 60 * 24 * 3);
		}
		// #endif
		console.log("ssssss", storeId)
		return storeId;
	},
	queryURLParams(url, paramName) {
		// 正则表达式模式，用于匹配URL中的参数部分。正则表达式的含义是匹配不包含 ?、&、= 的字符作为参数名，之后是等号和不包含 & 的字符作为参数值
		let pattern = /([^?&=]+)=([^&]+)/g;
		let params = {};

		// match用于存储正则匹配的结果
		let match;
		// while 循环和正则表达式 exec 方法来迭代匹配URL中的参数
		while ((match = pattern.exec(url)) !== null) {
			// 在字符串url中循环匹配pattern，并对每个匹配项进行解码操作，将解码后的键和值分别存储在key和value变量中
			let key = decodeURIComponent(match[1]);
			let value = decodeURIComponent(match[2]);

			if (params[key]) {
				if (Array.isArray(params[key])) {
					params[key].push(value);
				} else {
					params[key] = [params[key], value];
				}
			} else {
				// 参数名在 params 对象中不存在，将该参数名和对应的值添加到 params 对象中
				params[key] = value;
			}
		}

		if (!paramName) {
			// 没有传入参数名称, 返回含有所有参数的对象params
			return params;
		} else {
			if (params[paramName]) {
				return params[paramName];
			} else {
				return '';
			}
		}
	}
}