module.exports = {

	// 系统名称
	name: "QShop商城",

	// 必填: 后端api地址, 斜杠/结尾, 参照下面格式

	apiUrl: "https://apidemo.qshopcn.com/api/",
	// apiUrl: "http://localhost:5000/api/",

	//是否启用H5端多开
	// 启用后将通过获取子域名中url参数作为storeId;例如"http://localhost:8080/#/?storeId=1448d0f2e01143a9bdfa4634b543c945".
	enabledH5Multi: true,
}