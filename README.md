# QShop .Net+Vue+UniApp多商户小程序商城系统源码

### 项目介绍
QShop商城，是全新推出的一款轻量级、高性能、前后端分离的多店铺电商系统，支持微信小程序，前后端源码100%开源，完美支持二次开发，让您快速搭建个性化独立商城。 技术架构：.Net6、WebAPI、Swagger、NUnit、VUE、Element-UI、Ant Design Vue，专注轻量可持续稳定的高可用系统，可学习可商用。

### 技术特点

- 前后端完全分离 (互不依赖 开发效率高)
- 采用.Net6(跨平台 Windows/Linux都可发布)
- Uni-APP（开发跨平台应用的前端框架）
- Element-UI（简单易用应用广泛的UI组件库）
- Ant Design Vue（企业级中后台产品UI组件库）
- RBAC（基于角色的权限控制管理）
- 所有端代码开源 (服务端.net6、后台vue端、uniapp端)
- 源码中清晰中文注释 (小白也能看懂的代码)

    

### 页面展示
#### 前端展示
![前端展示](https://apidemo.qshopcn.com/wwwroot/docImg/show/showxiaochengxu01.png)
![前端展示](https://apidemo.qshopcn.com/wwwroot/docImg/show/showxiaochengxu02.png)
#### 后端展示
![多商户商城](https://apidemo.qshopcn.com/wwwroot/docImg/show/showadmin04.png)
![店铺首页](https://apidemo.qshopcn.com/wwwroot/docImg/show/showadmin01.png)
![自定义小程序/H5首页](https://apidemo.qshopcn.com/wwwroot/docImg/show/showadmin03.png)
![商品编辑](https://apidemo.qshopcn.com/wwwroot/docImg/show/showadmin02.png)



### 官方交流
- 官网:   [QShop官网](http://qshopcn.com/dashboard)
- QQ群:  **925862525**
- 微信群

     ![微信群](https://apidemo.qshopcn.com/wwwroot/docImg/show/contact_me_qrwxQun.png)


### 演示直达
- 商城后台演示 :http://demo.qshopcn.com
- 店铺用户名和密码：qshop qshop
- 平台用户名和密码：admin qshop
- 前端演示
![小程序码](https://apidemo.qshopcn.com/wwwroot/docImg/show/xiaochengxuma.jpg)




### 部署推荐环境

- Windows/CentOS 7.3
- IIS/Nginx 1.24.0
- .Net6/7/8
- SqlServer/MySQL 5.7+


### 开发部署
请查看 [使用文档](http://qshopcn.com/usagedoc/index)

### 贡献代码
欢迎大家贡献代码, **[如何贡献代码](https://gitee.com/QShopcn/QShop/wikis/%E8%B4%A1%E7%8C%AE%E4%BB%A3%E7%A0%81)** 

欢迎大家有问题提交Issues,感谢


- [查看QShop官网](http://qshopcn.com)
- [查看QShop源码](https://gitee.com/qiushuochina/QShop)
- QQ群: 925862525

### 开源不易，坚持更难！请star下吧







