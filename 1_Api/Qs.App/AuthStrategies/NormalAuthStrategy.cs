﻿// ***********************************************************************
// <summary>
// 普通用户授权策略
// </summary>
// ***********************************************************************

using System.Collections.Generic;
using System.Linq;
using Qs.App.Base;
using Qs.Comm;
using Qs.App.Response;
using Qs.Repository;
using Qs.Repository.Domain;
using Qs.Repository.Interface;
using Qs.App.SSO;

namespace Qs.App
{
    /// <summary>
    /// 普通用户授权策略
    /// </summary>
    public class NormalAuthStrategy :AppBaseString<ModelUser, QsDBContext>, IAuthStrategy
    {
        
        protected VmUser _user;
        protected ModelAdminUser _admin;

        private List<string> _userRoleIds;    //用户角色GUID
        private DbExtension _dbExtension;
        public NormalAuthStrategy(IUnitWork<QsDBContext> unitWork, IRepository<ModelUser, QsDBContext> repository, DbExtension dbExtension) : base(unitWork, repository, dbExtension, null)
        {
            _dbExtension = dbExtension;
        }
        public List<ModuleView> Modules
        {
            get {
                var moduleIds = UnitWork.Find<ModelRelevance>(
                    u =>(u.Key == Define.RoleModule && _userRoleIds.Contains(u.FirstId))).Select(u => u.SecondId).ToList();

                var modules = (from module in UnitWork.Find<ModelModule>(u =>moduleIds.Contains(u.Id)
                                                                        &&u.Status==(int)xEnum.ComStatus.Normal)
                    select new ModuleView
                    {
                        SortNo = module.SortNo,
                        Name = module.Name,
                        Code = module.Code,
                        Id = module.Id,
                        IconName = module.IconName,
                        Url = module.Url,
                        ParentId = module.ParentId,
                        ParentName = module.ParentName,
                        Status = module.Status
                    }).OrderBy(p => p.SortNo).ToList();

                var userElements = ModuleBtns;

                foreach (var module in modules)
                {
                    module.Elements = userElements.Where(u => u.ModuleId == module.Id).ToList();
                }

                return modules;
            }
        }

        /// <summary>
        /// 小程序店铺Id
        /// </summary>
        public string StoreId { get; set; }

        public List<ModuleElement> ModuleBtns
        {
            get
            {
                var elementIds = UnitWork.Find<ModelRelevance>(u =>(u.Key == Define.RoleElement && _userRoleIds.Contains(u.FirstId))).Select(u => u.SecondId);
                var userElements = UnitWork.Find<ModuleElement>(u => elementIds.Contains(u.Id));
                return userElements.ToList();
            }
        }

        public List<ModelRole> Roles
        {
            get { return UnitWork.Find<ModelRole>(u => _userRoleIds.Contains(u.Id)).ToList(); }
        }

        /// <summary>
        /// 用户-用户端登录
        /// </summary>
        public VmUser User
        {
            get { return _user; }
            set
            {
                _user = value;
                _userRoleIds = UnitWork.Find<ModelRelevance>(u => u.FirstId == _user.UserId && u.Key == Define.UserRole).Select(u => u.SecondId).ToList();
            }
        }

       

    }
}