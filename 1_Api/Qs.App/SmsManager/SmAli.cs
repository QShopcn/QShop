using System;
using   AlibabaCloud.SDK.Dysmsapi20170525;
using AlibabaCloud.OpenApiClient.Models;
using AlibabaCloud.SDK.Dysmsapi20170525.Models;
using Qs.App.SmsManager.Model;
using Qs.Comm;
using Qs.Repository.Vm;

namespace Qs.App;

/// <summary>
/// 阿里短信
/// </summary>
public class SmAli : ISmsHelper
{
    
    VmSettingSms _vm;
    /// <summary>
    /// 构造函数
    /// </summary>
    /// <param name="vm"></param>
    public SmAli(VmSettingSms vm)
    {
        _vm = vm;
    }

    /// <summary>
    /// 发送验证码
    /// </summary>
    /// <param name="phone">手机号</param>
    /// <param name="code">验证码</param>
    public ResApiSms SendPhoneCode(string phone, string code)
    {
        ResApiSms res = new ResApiSms();
        if (_vm.Scene.captcha.IsEnable)
        {
            Config config = new Config
            {
                AccessKeyId = _vm.Engine.Aliyun.AccessKeyId,
                AccessKeySecret =_vm.Engine.Aliyun.AccessKeySecret,
            };

            Client client = new Client(config);
            SendSmsRequest sendSmsRequest = new SendSmsRequest
            {
                PhoneNumbers = phone,
                SignName = _vm.Engine.Aliyun.Sign,
                TemplateCode = _vm.Scene.captcha.TemplateCode,
                //如{"name":"张三","number":"1390000****"}
                TemplateParam =xConv.ToJson(new
                {
                  code = code
                })
            };

            SendSmsResponse resSms= client.SendSms(sendSmsRequest);
            if (resSms.Body.Code!="OK")
            {
                throw new Exception(resSms.Body.Message);
            }
        }
        return res;
    }

    /// <summary>
    /// 发送订单通知
    /// </summary>
    /// <param name="phone"></param>
    /// <param name="orderNo"></param>
    /// <returns></returns>
    public ResApiSms SendNewOrderToStore(string phone,string orderNo)
    {  
        ResApiSms res = new ResApiSms();
        if (_vm.Scene.captcha.IsEnable)
        {
            Config config = new Config
            {
                AccessKeyId = _vm.Engine.Aliyun.AccessKeyId,
                AccessKeySecret =_vm.Engine.Aliyun.AccessKeySecret,
            };

            Client client = new Client(config);
            SendSmsRequest sendSmsRequest = new SendSmsRequest
            {
                PhoneNumbers = phone,
                SignName = _vm.Engine.Aliyun.Sign,
                TemplateCode = _vm.Scene.order_pay.TemplateCode,
                //如{"name":"张三","number":"1390000****"}
                TemplateParam =xConv.ToJson(new
                {
                    orderNo = orderNo
                })
            };
            SendSmsResponse resSms = client.SendSms(sendSmsRequest);
        }
        return res;
    }
    
}



