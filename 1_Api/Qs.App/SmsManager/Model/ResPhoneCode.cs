namespace Qs.App.SmsManager.Model;

public class ResPhoneCode
{
    /// <summary>
    /// 验证码Id
    /// </summary>
    public string VerifyCodeId { get; set; }
}