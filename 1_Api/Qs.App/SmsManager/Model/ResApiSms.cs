namespace Qs.App.SmsManager.Model;
/// <summary>
/// 短信返回
/// </summary>
public class ResApiSms
{
    /// <summary>
    /// 构造函数
    /// </summary>
    /// <param name="isSuccess"></param>
    /// <param name="message"></param>
    public ResApiSms(bool isSuccess=true,string message="")
    {
        IsSuccess = isSuccess;
        Message = message;
    }

    /// <summary>
    /// 是否成功
    /// </summary>
    public bool IsSuccess { get; set; } 
    /// <summary>
    /// 错误信息
    /// </summary>
    public string Message { get; set; }
}