﻿using System;
using System.Collections.Generic;
using System.Linq;

using Qs.App.Base;
using Qs.App.Interface;
using Qs.App.Request;
using Qs.App.Response;
using Qs.Comm;
using Qs.Repository;
using Qs.Repository.Base;
using Qs.Repository.Domain;
using Qs.Repository.Interface;
using Qs.Repository.Request;
using Qs.Repository.Response;

namespace Qs.App
{
    /// <summary>
    /// 应用层
    /// </summary>
    public class AppFileGroup : AppBaseString<ModelFileGroup, QsDBContext>
    {
        private AppRevelanceManager _revelanceApp;

        /// <summary>
        /// 构造函数
        /// </summary>
        public AppFileGroup(IUnitWork<QsDBContext> unitWork, IRepository<ModelFileGroup, QsDBContext> repository,
            AppRevelanceManager app, DbExtension dbExtension, IAuth auth) : base(unitWork, repository, dbExtension, auth)
        {
            _revelanceApp = app;
        }

        /// <summary>
        /// 加载列表
        /// </summary>
        public TableData Load(ReqQuFileGroup req)
        {
            //var loginContext = _auth.GetCurrentUser();
            var result = new TableData();
            result.Result = ListByWhere(req, true);
            result.Count = ListLinq(req).Count();
            return result;
        }

        /// <summary>
        /// 列表查询(不分页)
        /// </summary>
        public List<ModelFileGroup> ListByWhere(ReqQuFileGroup req, bool isPage = false)
        {
            IQueryable<ModelFileGroup> linq = ListLinq(req);
            List<ModelFileGroup> list = isPage ? linq.Skip((req.Page - 1) * req.Limit).Take(req.Limit).ToList() : linq.ToList();
            return list.OrderByDescending(p => p.CreateTime).ToList();
        }

        /// <summary>
        /// listLinq
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        public IQueryable<ModelFileGroup> ListLinq(ReqQuFileGroup req)
        {
            var linq = UnitWork.Find<ModelFileGroup>(p => true);
            if (!string.IsNullOrEmpty(req.Key))
            {
                linq = linq.Where(p => p.GroupName.Contains(req.Key));
            }
            return linq;
        }

        /// <summary>
        /// 新增或修改
        /// </summary>
        public void AddOrUpdate(ReqAuFileGroup req)
        {
            var model = xConv.CopyMapper<ModelFileGroup, ReqAuFileGroup>(req);
            var isNew = string.IsNullOrEmpty(model.Id) ? true : false;
            if (isNew)
            {
                model.CreateTime = DateTime.Now;
                Repository.Add(model);
            }
            else
            {
                Repository.Update(model);
            }
        }
        /// <summary>
        /// 新增或修改
        /// </summary>
        public void DeleteModel(string[] ids)
        {
            foreach (var id in ids)
            {
                var countSon = UnitWork.Count<ModelFileUpload>(p => p.GroupId == id);
                if (countSon <= 0)
                {
                    UnitWork.Delete<ModelFileGroup>(p => p.Id == id);
                    UnitWork.Save();
                }
                else
                {
                    throw new Exception("此分类,还有引用数据不可删除!");
                }
            }
        }
    }
}