﻿// ***********************************************************************
// Assembly         : Qs.App
// Author           : Qs
// Created          : 07-05-2018
//
// Last Modified By : Qs
// Last Modified On : 07-05-2018
// ***********************************************************************
// <copyright file="AuthContextFactory.cs" company="Qs.App">
//     Copyright (c) http://www.Qs.net.cn. All rights reserved.
// </copyright>
// <summary>
// 用户权限策略工厂
//</summary>
// ***********************************************************************

using Qs.App.AuthStrategies;
using Qs.Comm;
using Qs.Comm;
using Qs.Repository;
using Qs.Repository.Domain;
using Qs.Repository.Interface;

using static Qs.Comm.xEnum;

namespace Qs.App
{
    /// <summary>
    ///  加载用户所有可访问的资源/机构/模块
    /// <para>Qs新增于2021-07-19 10:53:30</para>
    /// </summary>
    public class AuthContextFactory
    {
        private SystemAuthStrategy _systemAuth;
        private NormalAuthStrategy _normalAuthStrategy;
        private readonly IUnitWork<QsDBContext> _unitWork;

        /// <summary>
        /// 构造方法
        /// </summary>
        /// <param name="sysStrategy"></param>
        /// <param name="normalAuthStrategy"></param>
        /// <param name="unitWork"></param>
        public AuthContextFactory(SystemAuthStrategy sysStrategy
            , NormalAuthStrategy normalAuthStrategy
            , IUnitWork<QsDBContext> unitWork)
        {
            _systemAuth = sysStrategy;
            _normalAuthStrategy = normalAuthStrategy;
            _unitWork = unitWork;
        }

        /// <summary>
        /// 获取登录上下文
        /// </summary>
        /// <param name="token"></param>
        /// <param name="appKey"></param>
        /// <returns></returns>
        public AuthStrategyContext GetAuthStrategyContext(string token, string appKey)
        {
            if (string.IsNullOrEmpty(token))
            {
                return null;
            };
            IAuthStrategy service = _normalAuthStrategy;
            if (appKey == Define.AppWebAdmin)//管理端
            {
                var adminUser = _unitWork.FirstOrDefault<ModelAdminUser>(u => u.Token == token);
                if (adminUser == null) { 
                return null;
                }
                if (adminUser.UserName == Define.SystemUserName)//超级管理员
                {
                    service = _systemAuth;
                }
                else
                {
                    var admin = _unitWork.FirstOrDefault<ModelAdminUser>(p => p.Token == token && p.TokenTime > System.DateTime.Now.AddDays(-Define.TokenDays));
                    if (admin == null) {
                        return null;
                    }
                    service.User = new SSO.VmUser (){
                        UserId=admin.Id,
                        Token=admin.Token,
                        NickName=admin.UserName  ,
                        UserType=(int)xEnum.UserType.StoreAdmin,
                        Status=xConv.ToInt(admin.Status),
                        StoreId = admin.StoreId,
                        AdminInfo=new SSO.AdminInfo() { 
                            UserName=admin.UserName
                        }
                    };
                }

            }
            else //用户
            {
                var user = _unitWork.FirstOrDefault<ModelUser>(p => p.Token == token && p.TokenTime > System.DateTime.Now.AddDays(-Define.TokenDays));
                if (user == null)
                {
                    return null;
                }
                service.User = new SSO.VmUser()
                {
                    UserId = user.Id,
                    Token = user.Token,
                    NickName = user.NickName,
                    UserType = xConv.ToInt(user.UserType),
                    Status = xConv.ToInt(user.Status),
                    StoreId = user.StoreId,
                    WxInfo = new SSO.WxInfo()
                    {
                        WxOpenId = user.WxOpenId,
                        IsBindPhone=string.IsNullOrEmpty(user.Phone)?false:true,
                        SourceUserId = user.SourceUserId,
                        UrlAvatar=user.UrlAvater
                    }
                }; 
            }

            return new AuthStrategyContext(service);
        }



    }
}