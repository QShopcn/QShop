using Qs.Comm;    

namespace Qs.App.SSO
{
    /// <summary>
    /// 登录
    /// </summary>
    public class VmUser :Response<string>
    {
        /// <summary>
        /// 用户Id/IM用户Id
        /// </summary>
        public string UserId;
        /// <summary>
        /// Token
        /// </summary>
        public string Token;
        /// <summary>
        /// 默认昵称
        /// </summary>
        public string NickName { get; set; }
        /// <summary>
        /// 用户类型( 10:用户 15:会员 20:司机 30:调度 40:业务)
        /// </summary>
        public int UserType { get; set; }
        /// <summary>
        ///状态 10:正常 -10:禁用
        /// </summary>
        public int Status { get; set; }
        /// <summary>
        /// 注册跳转路径
        /// </summary>
        public string ReturnUrl;
        /// <summary>
        /// 店铺Id
        /// </summary>
        public string StoreId { get; set; }

        /// <summary>
        /// 微信用户信息
        /// </summary>
        public WxInfo WxInfo { get; set; }
        /// <summary>
        /// 管理端用户信息
        /// </summary>
        public AdminInfo AdminInfo { get; set; }
       
    }

    /// <summary>
    /// 微信信息
    /// </summary>
    public class WxInfo {
        /// <summary>
        /// 微信OpenId
        /// </summary>
        public string WxOpenId { get; set; }
        /// <summary>
        /// 是否已绑定手机
        /// </summary>
        public bool IsBindPhone { get; set; }
        /// <summary>
        /// 用户来源
        /// </summary>
        public string SourceUserId { get;set; }

        /// <summary>
        /// 默认头像
        /// </summary>
        public string UrlAvatar { get; set; }

       
    }
    /// <summary>
    /// 管理端信息
    /// </summary>
    public class  AdminInfo
    {
        /// <summary>
        /// 用户名
        /// </summary>
        public string UserName { get; set; }
       
    }
}