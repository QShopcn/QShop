using System;

using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;

using Qs.App.AuthStrategies;
using Qs.App.Interface;
using Qs.Comm;
using Qs.Comm.Cache;
using Qs.Repository;
using Qs.Repository.Domain;
using Qs.Repository.Interface;
using Qs.Repository.Vm;

namespace Qs.App.SSO
{
    /// <summary>
    ///     使用本地登录。这个注入IAuth时，只需要Qs.Mvc一个项目即可，无需webapi的支持
    /// </summary>
    public class LocalAuth : IAuth
    {
        private readonly AuthContextFactory _appFactory;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly LoginParse _loginParse;
        private readonly IUnitWork<QsDBContext> _unitWork;
        private readonly AppCacheComm _appCacheComm;
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="httpContextAccessor"></param>
        /// <param name="app"></param>
        /// <param name="loginParse"></param>
        /// <param name="unitWork"></param>
        /// <param name="appCacheComm"></param>
        public LocalAuth(IHttpContextAccessor httpContextAccessor, AuthContextFactory app, LoginParse loginParse,
            IUnitWork<QsDBContext> unitWork, AppCacheComm appCacheComm)
        {
            _httpContextAccessor = httpContextAccessor;
            _appFactory = app;
            _loginParse = loginParse;
            _unitWork = unitWork;
            _appCacheComm = appCacheComm;
        }

        /// <summary>
        /// 是否已登录
        /// </summary>
        /// <param name="token"></param>
        /// <param name="otherInfo"></param>
        /// <returns></returns>
        public bool CheckLogin(string token = "", string otherInfo = "")
        {
            if (string.IsNullOrEmpty(token))
            {
                token = GetToken();
            }

            if (string.IsNullOrEmpty(token))
            {
                return false;
            }
            try
            {
                var result = GetUserByToken(token) != null;
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        /// <summary>
        /// 获取当前登录的用户信息
        /// <para>通过URL中的Token参数或Cookie中的Token</para>
        /// </summary>
        /// <returns></returns>
        public AuthStrategyContext GetCurrentContext()
        {
            var token = GetToken();
            var platform = GetPlatform();
            if (string.IsNullOrEmpty(token))     //为空随便给个值避免与库内token为""的数据
            {
                token = xConv.NewGuid();
            }
            AuthStrategyContext context = _appFactory.GetAuthStrategyContext(token, platform);

            return context;
        }

        /// <summary>
        ///     获取当前登录的用户名
        ///     <para>通过URL中的Token参数或Cookie中的Token</para>
        /// </summary>
        /// <param name="otherInfo">The account.</param>
        /// <returns>System.String.</returns>
        public string GetUserName(string otherInfo = "")
        {
            var user = GetUserByToken(GetToken());
            if (user != null)
            {
                return user.Phone;
            }

            return "";
        }

        /// <summary>
        ///     登录接口
        /// </summary>
        /// <param name="appKey">应用程序key.</param>
        /// <param name="username">用户名</param>
        /// <param name="pwd">密码</param>
        /// <param name="phoneCode">验证码</param>
        public VmUser LoginAdmin(string appKey, string username, string pwd, string phoneCode = "")
        {
            var result = _loginParse.DoAdmin(new PassportLoginRequest
            {
                AppKey = appKey,
                Account = username,
                Password = pwd,
                PhoneCode = phoneCode

            });
            return result;
        }
        /// <summary>
        /// 登录接口
        /// </summary>
        /// <param name="appKey">应用程序key.</param>
        /// <param name="wxOpenId"></param>
        /// <param name="storeId"></param>
        /// <returns>System.String.</returns>
        public VmUser LoginWx(string appKey, string wxOpenId, string storeId)
        {
            var result = _loginParse.DoWxOrPhone(new ReqLoginWx.ReqLoginWxDo
            {
                AppKey = appKey,
                WxOpenIdOrPhone = wxOpenId,
                StoreId = storeId
            });
            return result;
        }
        /// <summary>
        /// 登录接口
        /// </summary>
        /// <param name="appKey">应用程序key.</param>
        /// <param name="phone"></param>
        /// <param name="storeId"></param>
        /// <returns>System.String.</returns>
        public VmUser LoginByPhone(string appKey, string phone, string storeId)
        {
            var result = _loginParse.DoWxOrPhone(new ReqLoginWx.ReqLoginWxDo
            {
                AppKey = appKey,
                WxOpenIdOrPhone = phone,
                StoreId = storeId
            });
            return result;
        }


        /// <summary>
        ///     注销
        /// </summary>
        public bool Logout()
        {
            var token = GetToken();
            if (string.IsNullOrEmpty(token))
            {
                return true;
            }

            try
            {
                _appCacheComm.RemoveToken(token);
                _unitWork.Update<ModelUser>(p => p.Token == token, u => new ModelUser()
                {
                    Token = ""
                });
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        /// <summary>
        /// 获取Token
        /// </summary>
        /// <returns></returns>
        private string GetToken()
        {
            string token = _httpContextAccessor == null ? "" : _httpContextAccessor.HttpContext.Request.Query[Define.TokenName];
            if (!string.IsNullOrEmpty(token))
            {
                return token;
            }

            token = _httpContextAccessor.HttpContext.Request.Headers[Define.TokenName];
            if (!string.IsNullOrEmpty(token))
            {
                return token;
            }

            token = _httpContextAccessor.HttpContext.Request.Cookies[Define.TokenName];
            return token ?? string.Empty;
        }

        ///// <summary>
        /////获取登录用户店铺Id
        ///// </summary>
        ///// <returns></returns>
        //public string GetStoreId()
        //{

        //    var platform = GetPlatform();
        //    var storeId = _httpContextAccessor.HttpContext.Request.Headers["storeId"].ToString();
        //    if (platform == Define.AppWxApp && string.IsNullOrEmpty(storeId))
        //    {
        //        throw new Exception("小程序StoreId不能为空!");
        //    }
        //    if (platform == Define.AppWebAdmin)
        //    {
        //        var currentContext = GetCurrentContext();
        //        if (currentContext != null)
        //        {
        //            storeId = GetCurrentContext().User.StoreId;
        //        }
        //    }
        //    return storeId;
        //}

        /// <summary>
        ///获取登录用户店铺Id
        /// </summary>
        /// <returns></returns>
        public string GetStoreId()
        {
            var storeId = "";
            var platform = GetPlatform();
            var appId = _httpContextAccessor.HttpContext.Request.Headers["appId"].ToString();
            if (platform == Define.AppWxApp)
            {
                storeId = _appCacheComm.GetStoreId(appId);
            }
            if (platform == Define.AppH5 || platform == Define.AppWxH5)
            {
                storeId = _appCacheComm.GetStoreId(appId);
            }
            if (platform == Define.AppWebAdmin)
            {
                var currentContext = GetCurrentContext();
                if (currentContext != null)
                {
                    storeId = GetCurrentContext().User.StoreId;
                }
            }
            return storeId;
        }

        /// <summary>
        ///获取当前平台来源 (WebAdmin:管理后端 )
        /// </summary>
        /// <returns></returns>
        public string GetPlatform()
        {
            var platform = _httpContextAccessor.HttpContext.Request.Headers["platform"].ToString();
            return platform;
        }


        /// <summary>
        ///     根据Token获取用户
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        public ModelUser GetUserByToken(string token)
        {
            var user = _unitWork.FirstOrDefault<ModelUser>(p => p.Token == token && p.TokenTime > DateTime.Now.AddDays(-Define.TokenDays));
            return user;
        }
    }
}