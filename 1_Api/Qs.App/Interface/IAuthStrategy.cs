﻿// ***********************************************************************
// </copyright>
// <summary>
// 授权策略接口
// </summary>
// ***********************************************************************


using System.Collections.Generic;
using Qs.Comm;
using Qs.App.Response;
using Qs.Repository.Domain;
using Qs.App.SSO;

namespace Qs.App
{
    public interface IAuthStrategy 
    {
         List<ModuleView> Modules { get; }

        List<ModuleElement> ModuleBtns { get; }

        List<ModelRole> Roles { get; }

        /// <summary>
        ///用户 --其他端登录时才有值
        /// </summary>
        VmUser User
        {
            get;set;
        }
       
    }
}