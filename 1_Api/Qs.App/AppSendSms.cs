﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Qs.App.Base;
using Qs.App.Interface;
using Qs.App.SmsManager.Model;
using Qs.Comm;
using Qs.Comm.Cache;
using Qs.Repository;
using Qs.Repository.Domain;
using Qs.Repository.Interface;
using Qs.Repository.Vm;

namespace Qs.App
{
    /// <summary>
    /// 手机短信
    /// </summary>
    public class AppSendSms : AppBaseString<ModelUser, QsDBContext>
    {
        private AppStoreSetting _appSetting;
        private VmSettingSms smsSetting;
        private ISmsHelper sms;
        private AppCacheComm _appCacheComm;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="unitWork"></param>
        /// <param name="repository"></param>
        /// <param name="appSetting"></param>
        /// <param name="appCacheComm"></param>
        /// <param name="dbExtension"></param>
        /// <param name="auth"></param>
        public AppSendSms(IUnitWork<QsDBContext> unitWork, IRepository<ModelUser, QsDBContext> repository,
            AppStoreSetting appSetting, AppCacheComm appCacheComm, DbExtension dbExtension, IAuth auth) : base(unitWork, repository, dbExtension, auth)
        {
            //try
            //{
            _appSetting = appSetting;
            _appCacheComm = appCacheComm;
            smsSetting = _appSetting.GetDetail(DefineSetting.Sms) ?? new VmSettingSms();

            switch (smsSetting.Default)
            {
                case "qcloud"://腾讯云
                    sms = new SmsTx(smsSetting);
                    break;
                case "aliyun"://阿里云短信
                    sms = new SmAli(smsSetting);
                    break;
                case "qiniu":  //七牛云
                    break;
                default:
                    break;
            }
            //}
            //catch (Exception ex)
            //{
            //    xLog.Add(new Comm.Model.ModelLog()
            //    {
            //        Href = "AppSendSms",
            //        ApiInContent = $"ex:{ex.Message},{ex.StackTrace}"
            //    }, xEnum.LogLevel.Error);
            //    throw ex;
            //}
        }

        /// <summary>
        /// 发送验证码
        /// </summary>
        /// <param name="phone"></param>
        public ResPhoneCode SendCode(string phone)
        {
            if (sms == null)
            {
                throw new Exception("请先配置短信设置,店铺设置=>短信通知)");
            }
            string code = xConv.GenerateRandomCode(4);
            ResApiSms res = sms.SendPhoneCode(phone, code);
            string verifyCodeId = xConv.NewGuid();
            _appCacheComm.SetVerifyCode(verifyCodeId, code);
            ResPhoneCode resResult = new ResPhoneCode()
            {
                VerifyCodeId = verifyCodeId,
                // code = code,
            };
            return resResult;
        }

        /// <summary>
        /// 送新订单(到商户)
        /// </summary>
        /// <param name="phone"></param>
        /// <param name="orderNo"></param>
        public void SendNewOrderToStore(string phone, string orderNo)
        {
            sms.SendNewOrderToStore(phone, orderNo);
        }
    }
}
