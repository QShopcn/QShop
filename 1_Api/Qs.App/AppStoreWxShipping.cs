﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Qs.App.Base;
using Qs.App.Interface;
using Qs.Comm;
using Qs.Comm.Extensions;
using Qs.Repository;
using Qs.Repository.Base;
using Qs.Repository.Domain;
using Qs.Repository.Interface;
using Qs.Repository.Request;
using Qs.Repository.Response;

namespace Qs.App
{
    /// <summary>
    /// 微信发货信息 App
    /// </summary>
    public class AppStoreWxShipping : AppBaseString<ModelStoreWxShipping, QsDBContext>
    {
        private AppRevelanceManager _revelanceApp;
        
        /// <summary>
        /// 构造函数
        /// </summary>
        public AppStoreWxShipping(IUnitWork<QsDBContext> unitWork, IRepository<ModelStoreWxShipping, QsDBContext> repository,
            AppRevelanceManager app,DbExtension dbExtension, IAuth auth) : base(unitWork, repository, dbExtension, auth)
        {
            _revelanceApp = app;
        }
        
        /// <summary>
        /// 加载列表
        /// </summary>
        public TableData Load(ReqQuStoreWxShipping req)
        {           
            var result = new TableData();
            result.Result = ListByWhere(req,true);
            result.Count = ListLinq(req).Count();
            return result;
        }
        
        /// <summary>
        /// 列表查询(不分页)
        /// </summary>
        public List<ResStoreWxShipping> ListByWhere(ReqQuStoreWxShipping req, bool isPage = false)
        {
            IQueryable<ModelStoreWxShipping> linq = ListLinq(req);
            List<ModelStoreWxShipping> list = isPage ? linq.Skip((req.Page - 1) * req.Limit).Take(req.Limit).ToList() : linq.ToList();
            var listExpressWxCode = list.Select(p=>p.WxCode).ToList();
            var listExpress=UnitWork.Find<ModelStoreExpress>(p=> listExpressWxCode.Contains(p.WxCode)).ToList();
            List<ResStoreWxShipping> listRes = new List<ResStoreWxShipping>();
            foreach (var item in list) {
                listRes.Add(ResStoreWxShipping.ToView(item, listExpress));
            }
            return listRes;
        }

        /// <summary>
        /// listLinq
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        public IQueryable<ModelStoreWxShipping> ListLinq(ReqQuStoreWxShipping req)
        {
            var linq = UnitWork.Find<ModelStoreWxShipping>(p => true);
            if (!string.IsNullOrEmpty(req.Key))
            {
                linq = linq.Where(p => p.OrderNo.Contains(req.Key));
            }
             var storeId = _auth.GetStoreId();
            if (!string.IsNullOrEmpty(storeId))
            {
                linq = linq.Where(p => p.StoreId == storeId);
            }
            else if (req.OnlyStore)
            {
                var user = _auth.GetCurrentContext().User;
                linq = linq.Where(p => p.StoreId == user.StoreId);
            }
            return linq;
        }
        
        /// <summary>
        /// 新增或修改
        /// </summary>
        public void AddOrUpdate(ReqAuStoreWxShipping req)
        {
            var model = xConv.CopyMapper<ModelStoreWxShipping, ReqAuStoreWxShipping>(req);
            var isNew = string.IsNullOrEmpty(model.Id) ? true : false;
            var user = _auth.GetCurrentContext().User;
            model.StoreId = user.StoreId;
            if (isNew)
            {
                Repository.Add(model);
            }
            else
            {
                Repository.Update(model);
            }
        }
        
        /// <summary>
        /// GetDetail
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        public ModelStoreWxShipping GetDetail(string id)
        {
            var storeId = _auth.GetStoreId();           
            Expression<Func<ModelStoreWxShipping, bool>> exp = LambdaExtensions.True<ModelStoreWxShipping>();
            if (!string.IsNullOrEmpty(storeId))
            {
                exp = exp.And(p => p.StoreId == storeId);
            }
            if (!string.IsNullOrEmpty(id))
            {
                exp = exp.And(p => p.Id == id);
            }
            var model = UnitWork.FirstOrDefault<ModelStoreWxShipping>(exp);            
            return model;
        }
        
    }
}