﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

using Qs.App.Base;
using Qs.App.Interface;
using Qs.App.Request;
using Qs.Comm;
using Qs.Comm.Extensions;
using Qs.Repository;
using Qs.Repository.Base;
using Qs.Repository.Domain;
using Qs.Repository.Interface;
using Qs.Repository.Request;
using Qs.Repository.Response;

namespace Qs.App
{
    /// <summary>
    /// 管理端用户 App
    /// </summary>
    public class AppAdminUser : AppBaseString<ModelAdminUser, QsDBContext>
    {
        private AppRevelanceManager _revelanceApp;
        
        /// <summary>
        /// 构造函数
        /// </summary>
        public AppAdminUser(IUnitWork<QsDBContext> unitWork, IRepository<ModelAdminUser, QsDBContext> repository,
            AppRevelanceManager app,DbExtension dbExtension, IAuth auth) : base(unitWork, repository, dbExtension, auth)
        {
            _revelanceApp = app;
        }
        
        /// <summary>
        /// 加载列表
        /// </summary>
        public TableData Load(ReqQuAdminUser req)
        {           
            var result = new TableData();
            result.Result = ListByWhere(req,true);
            result.Count = ListLinq(req).Count();
            return result;
        }
        
        /// <summary>
        /// 列表查询(不分页)
        /// </summary>
        public List<ModelAdminUser> ListByWhere(ReqQuAdminUser req, bool isPage = false)
        {
            IQueryable<ModelAdminUser> linq = ListLinq(req);
            List<ModelAdminUser> list = isPage ? linq.Skip((req.Page - 1) * req.Limit).Take(req.Limit).ToList() : linq.ToList();
            return list;
        }

        /// <summary>
        /// listLinq
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        public IQueryable<ModelAdminUser> ListLinq(ReqQuAdminUser req)
        {
            var linq = UnitWork.Find<ModelAdminUser>(p => true);
            if (!string.IsNullOrEmpty(req.Key))
            {
                linq = linq.Where(p => p.UserName.Contains(req.Key));
            }
            var storeId = _auth.GetStoreId();
            if (!string.IsNullOrEmpty(storeId))
            {
                linq = linq.Where(p => p.StoreId == storeId);
            }
            else if (req.OnlyStore)
            {
                var user = _auth.GetCurrentContext().User;
                linq = linq.Where(p => p.StoreId == user.StoreId);
            }
            return linq;
        }
        
        /// <summary>
        /// 新增或修改
        /// </summary>
        public void AddOrUpdate(ReqAuAdminUser req)
        {
            var model = xConv.CopyMapper<ModelAdminUser, ReqAuAdminUser>(req);
            var isNew = string.IsNullOrEmpty(model.Id) ? true : false;
            var user = _auth.GetCurrentContext().User;
            model.StoreId = user.StoreId;
            if (isNew)
            {
                Repository.Add(model);
            }
            else
            {
                Repository.Update(model);
            }
        }
        
        /// <summary>
        /// GetDetail
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        public ModelAdminUser GetDetail(string id)
        {
            var storeId = _auth.GetStoreId();           
            Expression<Func<ModelAdminUser, bool>> exp = LambdaExtensions.True<ModelAdminUser>();
            if (!string.IsNullOrEmpty(storeId))
            {
                exp = exp.And(p => p.StoreId == storeId);
            }
            if (!string.IsNullOrEmpty(id))
            {
                exp = exp.And(p => p.Id == id);
            }
            var model = UnitWork.FirstOrDefault<ModelAdminUser>(exp);            
            return model;
        }


        /// <summary>
        /// 获取指定角色包含的管理用户列表
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        public async Task<TableData> LoadByRole(QueryUserListByRoleReq req)
        {
            var listAdmin = from userRole in UnitWork.Find<ModelRelevance>(u =>u.SecondId == req.roleId && u.Key == Define.UserRole)
                        join admin in UnitWork.Find<ModelAdminUser>(null) on userRole.FirstId equals admin.Id into temp
                        from c in temp.Where(u => u.Id != null)
                        select c;

            return new TableData
            {
                Count = listAdmin.Count(),
                Result = listAdmin.Skip((req.Page - 1) * req.Limit).Take(req.Limit)
            };
        }
    }
}