﻿using Qs.App.Base;
using Qs.Repository.Base;

namespace Qs.App.Request
{
    public class QueryRoleListReq : BaseReqPage
    {
        /// <summary>
        /// 只查询当前商户
        /// </summary>
        public bool OnlyStore { get; set; } = true;
    }
}
