﻿using Qs.Comm;
using Qs.Repository.Domain;
using Qs.Repository.Response;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qs.Repository.Vm
{
    /// <summary>
    /// 
    /// </summary>
    public class VmWxPaySetting : WxPayObj
    {
        /// <summary>
        /// 微信AppId
        /// </summary>
        public string WxAppId { get; set; }
        /// <summary>
        /// 微信AppId
        /// </summary>
        public string WxAppSecret { get; set; }
        /// <summary>
        /// 店铺Id
        /// </summary>
        public string StoreId { get; set; }

        /// <summary>
        /// View
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public static VmWxPaySetting ToView(ModelStoreSettingPay model, VmSettingBasicWxApp wxAppSetting)
        {
            ResStoreSettingPay res = xConv.CopyMapper<ResStoreSettingPay, ModelStoreSettingPay>(model);
            WxPayObj obj = xConv.JsonToObj<WxPayObj>(res.Values); ;
            VmWxPaySetting vm = xConv.CopyMapper<VmWxPaySetting, WxPayObj>(obj);
           
            vm.StoreId = model.StoreId;
            vm.WxAppId = wxAppSetting.AppId;
            vm.WxAppSecret=wxAppSetting.AppSecret;
            return vm;
        }
    }
}
