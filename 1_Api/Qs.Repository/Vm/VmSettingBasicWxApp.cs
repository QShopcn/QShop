﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qs.Repository.Vm
{
    /// <summary>
    /// 小程序设置
    /// </summary>
    public class VmSettingBasicWxApp
    {
        /// <summary>
        /// 店铺Id
        /// </summary>
        public string StoreId { get; set; }
        /// <summary>
        /// 微信小程序ID
        /// </summary>
        public string AppId { get; set; }
        /// <summary>
        ///  微信小程序-AppSecret
        /// </summary>
        public string AppSecret { get; set; }

    }
}

