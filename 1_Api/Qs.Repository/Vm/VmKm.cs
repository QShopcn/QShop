﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qs.Repository.Vm
{
    public class VmDistanceKm
    {
        /// <summary>
        /// 距离KM
        /// </summary>
        public  double Distance { get; set; }
    }
}
