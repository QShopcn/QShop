﻿//------------------------------------------------------------------------------
// <autogenerated>
//     This code was generated by a CodeSmith Template.
//
//     DO NOT MODIFY contents of this file. Changes to this
//     file will be lost if the code is regenerated.
// </autogenerated>
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Qs.Repository.Core;

namespace Qs.Repository.Request
{
    /// <summary>
	/// 用户余额变动明细表
	/// </summary>
    [Table("tb_UserBalanceLog")]
    public partial class ReqAuUserBalanceLog 
    {

        /// <summary>
        /// 主键Id
        /// </summary>
        public string Id { get; set; }
        /// <summary>
        /// 用户Id
        /// </summary>
        public string UserId { get; set; }
        /// <summary>
        /// 订单ID(可选)
        /// </summary>
        public string OrderId { get; set; }
        /// <summary>
        /// 余额变动场景(10用户充值 20用户消费 30管理员操作 40订单退款)
        /// </summary>
        public int Scene { get; set; }
        /// <summary>
        /// 变动金额
        /// </summary>
        public decimal Money { get; set; }
        /// <summary>
        /// 操作后余额
        /// </summary>
        public decimal? Balance { get; set; }
        /// <summary>
        /// 描述/说明
        /// </summary>
        public string Describe { get; set; }
        /// <summary>
        /// 管理员备注
        /// </summary>
        public string Remark { get; set; }
        /// <summary>
        /// 店铺Id
        /// </summary>
        public string StoreId { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        public System.DateTime CreateTime { get; set; }
    }
}