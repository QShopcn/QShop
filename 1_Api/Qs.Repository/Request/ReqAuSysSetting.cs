﻿//------------------------------------------------------------------------------
// <autogenerated>
//     This code was generated by a CodeSmith Template.
//
//     DO NOT MODIFY contents of this file. Changes to this
//     file will be lost if the code is regenerated.
// </autogenerated>
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Qs.Repository.Core;

namespace Qs.Repository.Request
{
    /// <summary>
	/// 系统设置
	/// </summary>
    [Table("tb_SysSetting")]
    public partial class ReqAuSysSetting 
    {

        /// <summary>
        /// 
        /// </summary>
        public string Id { get; set; }
        /// <summary>
        /// 设置项标示
        /// </summary>
        public string Key { get; set; }
        /// <summary>
        /// 设置项描述
        /// </summary>
        public string Describe { get; set; }
        /// <summary>
        /// 设置内容（json格式）
        /// </summary>
        public string Values { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        public System.DateTime CreateTime { get; set; }
    }
}