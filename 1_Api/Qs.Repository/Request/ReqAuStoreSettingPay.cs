﻿//------------------------------------------------------------------------------
// <autogenerated>
//     This code was generated by a CodeSmith Template.
//
//     DO NOT MODIFY contents of this file. Changes to this
//     file will be lost if the code is regenerated.
// </autogenerated>
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

using Qs.Comm;
using Qs.Repository.Core;
using Qs.Repository.Response;

namespace Qs.Repository.Request
{
    /// <summary>
	/// 支付设置
	/// </summary>
    public partial class ReqAuStoreSettingPay
    {

        /// <summary>
        /// Id
        /// </summary>
        public string Id { get; set; }
        /// <summary>
        /// 编码
        /// </summary>
        public string Code { get; set; }
        /// <summary>
        /// 名字
        /// </summary>
        public string Name { get; set; }
       
        /// <summary>
        /// 微信支付设置
        /// </summary>
        public WxPayObj SettingObj { get; set; }

        public void Check()
        {
            xValidation.CheckStrNull(Name, "Name");
            if (Code == "WxPay")
            {
                SettingObj.Check();
            }
        }
    }
}