﻿namespace Qs.Repository.Request
{
    /// <summary>
    /// 生成邀请海报
    /// </summary>
    public class ReqGenInvitePoster
    {
        /// <summary>
        /// 海报ID
        /// </summary>
        public string PosterId { get; set; }
    }
}
