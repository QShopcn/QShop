﻿//------------------------------------------------------------------------------
// <autogenerated>
//     This code was generated by a CodeSmith Template.
//
//     DO NOT MODIFY contents of this file. Changes to this
//     file will be lost if the code is regenerated.
// </autogenerated>
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Qs.Repository.Core;

namespace Qs.Repository.Request
{
    /// <summary>
	/// 微信发货信息
	/// </summary>
    public partial class ReqAuStoreWxShipping 
    {

        /// <summary>
        /// Id
        /// </summary>
        public string Id { get; set; }
        /// <summary>
        /// 订单号
        /// </summary>
        public string OrderNo { get; set; }
        /// <summary>
        /// 物流方式(1:快递物流 2:虚拟发货 3:自提订单)
        /// </summary>
        public int? ShippingType { get; set; }
        /// <summary>
        /// 微信物流公司Code
        /// </summary>
        public string WxCode { get; set; }
        /// <summary>
        /// 快递鸟Code
        /// </summary>
        public string KuaiDiNiaoCode { get; set; }
        /// <summary>
        /// 快递100Code
        /// </summary>
        public string KuaiDi100Code { get; set; }
        /// <summary>
        /// 物流单号
        /// </summary>
        public string ExpressNo { get; set; }
        /// <summary>
        /// 微信发货错误信息
        /// </summary>
        public string ErrMsg { get; set; }
        /// <summary>
        /// 是否已微信发货(-10:发货错误 10:已发货)
        /// </summary>
        public int? Status { get; set; }
        /// <summary>
        /// 收件人联系方式
        /// </summary>
        public string ReceiverPhone { get; set; }
        /// <summary>
        /// 微信OpenId
        /// </summary>
        public string OpenId { get; set; }
        /// <summary>
        /// 用户Id
        /// </summary>
        public string UserId { get; set; }
        /// <summary>
        /// 店铺Id
        /// </summary>
        public string StoreId { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        public System.DateTime? CreateTime { get; set; }
    }
}