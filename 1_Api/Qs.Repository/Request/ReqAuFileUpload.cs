﻿//------------------------------------------------------------------------------
// <autogenerated>
//     This code was generated by a CodeSmith Template.
//
//     DO NOT MODIFY contents of this file. Changes to this
//     file will be lost if the code is regenerated.
// </autogenerated>
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Microsoft.AspNetCore.Http;
using Qs.Repository.Core;

namespace Qs.Repository.Request
{
    /// <summary>
	/// 文件
	/// </summary>
    [Table("FileUpload")]
    public partial class ReqAuFileUpload 
    {
        /// <summary>
        /// 文件列表
        /// </summary>
        public IFormFileCollection Files { get; set; }
        /// <summary>
        /// 分类Id
        /// </summary>
        public string GroupId { get; set; }
        /// <summary>
        /// 上传渠道
        /// </summary>
        public int? Channel { get; set; }
            
    }
}