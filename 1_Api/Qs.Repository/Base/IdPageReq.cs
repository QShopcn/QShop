﻿using Qs.Repository.Base;

namespace Qs.App.Request
{
    public class IdPageReq :BaseReqPage
    {
        public string id { get; set; }
    }
}
