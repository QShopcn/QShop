﻿using System.Collections.Generic;

namespace Qs.Repository.Response
{
    /// <summary>
    /// 数据统计
    /// </summary>
    public class ResStatisticEchartTable
    {
        /// <summary>
        /// 七日交易走势
        /// </summary>
        public List<VmEchart> ListEchart { get; set; }

        /// <summary>
        /// 商品销售榜
        /// </summary>
        public List<SalesGoods> ListSales {  get; set; }

        /// <summary>
        /// 用户消费榜
        /// </summary>
        public List<Consumers> ListConsumers { get; set; }
    }

    public class SalesGoods
    {
        /// <summary>
        /// 排名
        /// </summary>
        public int? SortNo { get; set; }
        /// <summary>
        /// 商品名称
        /// </summary>
        public string GoodsName { get; set; }
        /// <summary>
        /// 销售件
        /// </summary>
        public int SaleCount { get; set; }
    }

    /// <summary>
    /// 用户消费榜
    /// </summary>
    public class Consumers
    {
        /// <summary>
        /// 排名
        /// </summary>
        public int? SortNo { get; set; }
        /// <summary>
        /// 商品名称
        /// </summary>
        public string UserName { get; set; }
        /// <summary>
        /// 实际消费金额
        /// </summary>
        public decimal SumAmount { get; set; }
    }
}
