﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Qs.Comm;
using Qs.Repository.Domain;

namespace Qs.Repository.Response
{
    /// <summary>
    /// 分类
    /// </summary>
   public class ResGoodsCate :ModelGoodsCate
    {
        /// <summary>
        /// 缩略图
        /// </summary>
        public string UrlThumbnail { get; set; }

        /// <summary>
        /// 商城名称
        /// </summary>
        public string StoreName { get; set; }

        /// <summary>
        /// 分类图片列表
        /// </summary>
        public List<ModelFileUpload> ListFile{ get; set; }

        /// <summary>
        /// 转为Res
        /// </summary>
        /// <param name="model"></param>
        /// <param name="ListFile"></param>
        /// <returns></returns>
        public static ResGoodsCate ToView(ModelGoodsCate model, List<ModelFileUpload> ListFile) {
            ResGoodsCate res = xConv.CopyMapper<ResGoodsCate, ModelGoodsCate>(model);
            res.UrlThumbnail = ListFile.FirstOrDefault().Thumbnail;
            res.ListFile = ListFile;
            return res;
        }
    }
}
