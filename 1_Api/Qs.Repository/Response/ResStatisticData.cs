﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qs.Repository.Response
{
    /// <summary>
    /// 
    /// </summary>
    public class ResStatisticData
    {
        /// <summary>
        /// 成交金额
        /// </summary>
        public decimal AmountReal { get; set; }
        /// <summary>
        /// 支付订单数
        /// </summary>
        public int CountOrder { get; set; }
        /// <summary>
        /// 用户数
        /// </summary>
        public int CountUser { get; set; }
        /// <summary>
        /// 消费人数
        /// </summary>
        public int CountConsumers { get; set; }
        /// <summary>
        /// 用户充值总额
        /// </summary>
        public decimal AmountRecharge { get; set; }
    }
}
