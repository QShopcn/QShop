﻿using Qs.Comm;
using Qs.Repository.Domain;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Qs.Repository.Response
{
    /// <summary>
    /// response对象
    /// </summary>
    public class ResStoreWxShipping :ModelStoreWxShipping
    {
        /// <summary>
        /// 状态
        /// </summary>
        public string StrStatus { get; set; }

        /// <summary>
        /// 物流方式
        /// </summary>
        public string StrShippingType { get; set; }
        /// <summary>
        /// 物流公司
        /// </summary>
        public string ExpressName { get; set; }

        /// <summary>
        /// ToView
        /// </summary>
        /// <param name="model"></param>
        /// <param name="listStoreExpress"></param>
        /// <returns></returns>
        public static ResStoreWxShipping ToView(ModelStoreWxShipping model, List<ModelStoreExpress> listStoreExpress) {
            ResStoreWxShipping res = xConv.CopyMapper<ResStoreWxShipping, ModelStoreWxShipping>(model);
            res.StrStatus = xEnum.GetEnumDescription(typeof(xEnum.WxShippingStatus), model.Status); 
            res.StrShippingType = xEnum.GetEnumDescription(typeof(xEnum.ShippingType), model.ShippingType);
            res.ExpressName = (listStoreExpress.FirstOrDefault(p => p.WxCode == model.WxCode) ?? new ModelStoreExpress()).ExpressName;
            return res;
        }
    }
}
