﻿using Qs.Comm;
using Qs.Repository.Domain;
using Qs.Repository.Vm;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using TencentCloud.Scf.V20180416.Models;

namespace Qs.Repository.Response
{
    public class ResStoreSettingPay:ModelStoreSettingPay
    {
        public WxPayObj SettingObj { get; set; }

        /// <summary>
        /// View
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public static ResStoreSettingPay ToView(ModelStoreSettingPay model)
        {
            ResStoreSettingPay res = xConv.CopyMapper<ResStoreSettingPay, ModelStoreSettingPay>(model);
            res.SettingObj = xConv.JsonToObj<WxPayObj>(res.Values);
            return res;
        }
    }

  
    public class WxPayObj
    {
        
        /// <summary>
        /// 微信支付商户号
        /// </summary>
        public string PartnerId { get; set; }
        /// <summary>
        /// 微信支付秘钥
        /// </summary>
        public string PartnerKey { get; set; }
        /// <summary>
        /// 是否服务商支付
        /// </summary>
        public bool ServicePay { get; set; } = false;
        /// <summary>
        /// 子商户号
        /// </summary>
        public string SubPartnerId { get; set; }
        /// <summary>
        /// apiclient_cert.pem证书
        /// </summary>
        public string CertPath { get; set; }
        /// <summary>
        /// apiclient_key.pe 证书
        /// </summary>
        public string KeyPath { get; set; }
        public void Check()
        {
            if (ServicePay==true)
            {
                xValidation.CheckStrNull(SubPartnerId, "子商户号");
            }
            xValidation.CheckStrNull(PartnerId, "微信支付商户号");
            xValidation.CheckStrNull(PartnerKey, "微信支付秘钥");
            xValidation.CheckStrNull(CertPath, "apiclient_cert.pem证书");
            xValidation.CheckStrNull(KeyPath, "apiclient_key.pem证书");
        }
    }
}
