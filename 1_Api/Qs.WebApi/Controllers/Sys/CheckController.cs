﻿// ***********************************************************************
// Assembly         : Qs.WebApi
// Author           : Qs
// Created          : 07-11-2016
//
// Last Modified By : Qs
// Last Modified On : 07-11-2016
// Contact :
// File: CheckController.cs
// 登录相关的操作
// ***********************************************************************

using Qs.Comm;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Qs.App;
using Qs.App.Interface;
using Qs.App.Response;
using Qs.App.SSO;
using Qs.Repository.Domain;
using System;
using System.Collections.Generic;
using Microsoft.Extensions.Logging;
using Qs.App.AuthStrategies;
using Qs.App.Request.ReqApi;
using Qs.App.SmsManager.Model;
using Qs.App.UserManager;
using Qs.App.Wx;
using Qs.Comm.Cache;
using Qs.Comm.Helpers;
using Qs.Comm.VerifyCode;
using Qs.Repository.Request;
using Qs.Repository.Vm;
using Qs.Repository.Wx;

namespace Qs.WebApi.Controllers
{
    /// <inheritdoc />
    /// <summary>
    /// 登录及与登录信息获取相关的接口
    /// </summary>
    [Route("api/[controller]/[action]")]
    [ApiController]
    // [ApiExplorerSettings(GroupName = "登录验证_Check")]
    public class CheckController : ControllerBase
    {
        private readonly IAuth _auth;
        private AuthStrategyContext _authStrategyContext;
        private AppUserManager _appUser;
        private AppModule _appModule;
        private AppStoreSettingPay _appSettingPay;
        private AppStoreSetting _appSetting;
        private ICacheContext _cacheContext;
        private AppSendSms _appSendSms;
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="authUtil"></param>
        /// <param name="appUser"></param>
        /// <param name="appModule"></param>
        /// <param name="appSettingPay"></param>
        /// <param name="appSetting"></param>
        /// <param name="appSendSms"></param>
        /// <param name="cacheContext"></param>
        public CheckController(IAuth authUtil, AppUserManager appUser, AppModule appModule, AppStoreSettingPay appSettingPay,
            AppStoreSetting appSetting,AppSendSms appSendSms, ICacheContext cacheContext)
        {
            _auth = authUtil;
            _appUser = appUser;
            _appSettingPay = appSettingPay;
            _appSetting = appSetting;
            _authStrategyContext = _auth.GetCurrentContext();
            _cacheContext = cacheContext;
            _appSendSms = appSendSms;
            _appModule = appModule;
        }
        
        /// <summary>
        /// 获取登录用户资料
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public Response<UserView> GetUserProfile()
        {
            var resp = new Response<UserView>();
            try
            {
                resp.Result = _authStrategyContext.User.MapTo<UserView>();
            }
            catch (Exception e)
            {
                resp.Code = 500;
                resp.Message = e.Message;
            }

            return resp;
        }


        /// <summary>
        /// 获取登录用户的所有可访问的角色
        /// </summary>
        [HttpGet]
        public Response<List<ModelRole>> GetRoles()
        {
            var result = new Response<List<ModelRole>>();
            result.Result = _authStrategyContext.Roles;
            return result;
        }

        /// <summary>
        /// 获取所有模块及按钮列表
        /// </summary>
        [HttpGet]
        public Response<List<ModelModule>> GetAllModules()
        {
            var result = new Response<List<ModelModule>>();
            result.Result = _appModule.GetAllModules();
            return result;
        }
        /// <summary>
        /// 获取当前用户可访问模块及按钮列表
        /// </summary>
        [HttpGet]
        public Response<List<ModuleView>> GetCurrentModules()
        {
            var result = new Response<List<ModuleView>>();
            result.Result = _authStrategyContext.Modules;
            return result;
        }

        /// <summary>
        /// 获取可访问的模块及按钮树状结构
        /// </summary>
        [HttpGet]
        public Response<IEnumerable<TreeItem<ModuleView>>> GetModulesTree()
        {
            var result = new Response<IEnumerable<TreeItem<ModuleView>>>();

            result.Result = _authStrategyContext.Modules.GenerateTree(u => u.Id, u => u.ParentId);
            return result;
        }

        /// <summary>
        /// 根据token获取用户名称
        /// </summary>
        [HttpGet]
        public Response<string> GetUserName()
        {
            var result = new Response<string>();
            result.Result = _authStrategyContext.User.NickName;
            return result;
        }

        /// <summary>
        /// 微信小程序登录接口
        /// </summary>
        /// <param name="req">登录参数</param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        public Response<VmUser>  LoginWx([FromBody] WxAppLoginRequest req)
        {
            Response<VmUser> res = new Response<VmUser>();
            var storeId=_auth.GetStoreId();
            res.Result= new VmUser();
            req.Trim();
            VmSettingBasicWxApp setting = _appSetting.GetDetail(DefineSetting.BasicWxApp);
            if (string.IsNullOrEmpty(setting.AppId))
            {
                throw new Exception("请联系管理员,配置小程序设置信息!");
            }
            var resultWx = Code2Session.Get(req.WxCode, setting);
        
            if (xConv.ToInt(resultWx.errcode) == 0)
            {
                //新增修改用户
                ModelUser user = _appUser.AddOrUpdateWxUser(resultWx.openid,"", req.InviteCode);
                res.Result = _auth.LoginWx(Define.AppWxApp, resultWx.openid, storeId);
            }
            return res;
        }

        /// <summary>
        /// 登录接口-短信验证码登录
        /// </summary>
        /// <param name="req">登录参数</param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        public Response<VmUser> LoginByPhoneCode(ReqLoginByPhone req)
        {
            req.Check();
            var context = _auth.GetCurrentContext();
            var platform = _auth.GetPlatform();
            var storeId = _auth.GetStoreId();
            var res = new Response<VmUser>();
            ModelUser user = new ModelUser();
            //ValidateCode(xEnum.CodeType.Sms, req.SmsVerifyCodeId, req.SmsCode);
            VmUser vm = _appUser.AddOrUpdateByPhone(req.Mobile, req.InviteCode);
            res.Result = vm;
            return res;
        }

        /// <summary>
        /// 绑定微信小程序手机号码
        /// </summary>
        [HttpPost]
        [AllowAnonymous]
        public Response<VmUser> BindWxPhone([FromBody] ReqBindPhone req)
        {
            var result = new Response<VmUser>();
            var storeId = _auth.GetStoreId();
            req.Trim();
            var userId = _auth.GetCurrentContext().User.UserId;
            var user = _appUser.Get(userId);
            VmSettingBasicWxApp setting = _appSetting.GetDetail(DefineSetting.BasicWxApp);
            Code2Session.ResWxPhone resPhone = Code2Session.GetWxPhone(req.WxCode,  setting);
            string phone = resPhone.phone_info.purePhoneNumber; //无区号手机号
            VmUser vm = _appUser.AddOrUpdateByPhone(phone,"");
            result.Result = vm;
            return result;
        }

        /// <summary>
        /// 生成验证码 图片
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpGet]
        public Response<ResCaptcha> GetImageCaptcha()
        {
            Response<ResCaptcha> res = new Response<ResCaptcha>();
            ResCaptcha obj= GenVerCode(xEnum.VerifyCodeType.Arith);
            res.Result = obj;
            return res;
        }

        /// <summary>
        /// 获取手机验证码
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPost]
        public Response<ResPhoneCode> SendSmsCaptcha(ReqSendSmsCaptcha req)
        {
            req.Check();
            Response<ResPhoneCode> res = new Response<ResPhoneCode>();
            ValidateCode(xEnum.CodeType.Image,req.VerifyCodeId, req.VerifyCode);
            ResPhoneCode objResult = _appSendSms.SendCode(req.Mobile);
            res.Result = objResult;
            return res;
        }

        /// <summary>
        /// 登录接口
        /// </summary>
        /// <param name="req">登录参数</param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        public Response<VmUser> LoginAdmin(PassportLoginRequest req)
        {
            var result = new Response<VmUser>();
            try
            {
                req.Trim();
                ValidateCode(xEnum.CodeType.Sms,req.VerifyCodeId, req.VerifyCode);
                result.Result = _auth.LoginAdmin(req.AppKey, req.Account, req.Password, req.PhoneCode);
                result.Code = result.Result.Code;
                result.Message = result.Result.Message;
            }
            catch (Exception ex)
            {
                result.Code = 500;
                result.Message = ex.Message;
            }

            return result;
        }

        /// <summary>
        /// 生成验证码
        /// </summary>
        /// <returns></returns>
        private ResCaptchaCode GenVerCode(xEnum.VerifyCodeType verifyCodeType)
        {
            ModelVerifyCode codeInfo = VerifyCodeHelper.Captcha(verifyCodeType);
            string verifyCodeId = xConv.NewGuid();
            _cacheContext.Set(verifyCodeId, codeInfo.Code, DateTime.Now.AddMinutes(5));
            ResCaptchaCode obj = new ResCaptchaCode()
            {
                Base64Str = codeInfo.Base64Str,
                VerifyCodeId = verifyCodeId ,
            };
            return obj;
        }

        /// <summary>
        /// 验证验证码
        /// </summary>
        /// <param name="codeType">验证码类型</param>
        /// <param name="verifyCodeId">验证码Id</param>
        /// <param name="imageVerifyCode">验证码</param>
        private void ValidateCode(xEnum.CodeType codeType, string verifyCodeId, string imageVerifyCode)
        {
            var verifyCode = _cacheContext.Get<string>(verifyCodeId);
            _cacheContext.Remove(verifyCodeId);

            if (verifyCode == null)
            {
                if (codeType == xEnum.CodeType.Image)
                {
                    throw new Exception("验证码失效,请点击图片重新获取!");
                }

                else
                {
                    throw new Exception("验证码失效,请重新获取!");
                }
            }

            if (imageVerifyCode.ToLower() != verifyCode.ToLower())
            {
                throw new Exception("验证码错误!");
            }
        }

        /// <summary>
        /// 验证登录是否有效
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public Response<bool> CheckToken()
        {
            var result = new Response<bool>();
            try
            {
                result.Result = _auth.CheckLogin();
            }
            catch (Exception ex)
            {
                result.Code = DefineErrCode.InvalidToken;
                result.Message = ex.Message;
            }

            return result;
        }

        /// <summary>
        /// 注销登录
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public Response<bool> Logout()
        {
            var resp = new Response<bool>();
            try
            {
                resp.Result = _auth.Logout();
            }
            catch (Exception e)
            {
                resp.Result = false;
                resp.Message = e.Message;
            }   
            return resp;
        }
    }
}