﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Mvc;
using Qs.App;
using Qs.App.Request;
using Qs.Comm;
using Qs.Repository.Base;
using Qs.Repository.Domain;
using Qs.Repository.Request;
using Qs.Repository.Response;

namespace Qs.WebApi.Controllers.User
{
    /// <summary>
    /// 管理端用户 
    /// </summary>
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class AdminUserController : ControllerBase
    {
        private readonly AppAdminUser _app;

        /// <summary>
        /// /构造函数
        /// </summary>
        public AdminUserController(AppAdminUser app)
        {
            _app = app;
        }

        /// <summary>
        /// 列表查询(分页)
        /// </summary>
        [HttpGet]
        public TableData Load([FromQuery] ReqQuAdminUser req)
        {
            return _app.Load(req);
        }

        /// <summary>
        /// 列表查询(不分页,page,limit不需要传)
        /// </summary>
        [HttpGet]
        public Response<List<ModelAdminUser>> ListByWhere([FromQuery] ReqQuAdminUser req)
        {
            Response<List<ModelAdminUser>> res = new Response<List<ModelAdminUser>>();
            res.Result = _app.ListByWhere(req);
            return res;
        }

        /// <summary>
        /// 获取详情
        /// </summary>
        [HttpGet]
        public Response<ModelAdminUser> Get(string id)
        {
            var result = new Response<ModelAdminUser>();
            result.Result = _app.Get(id);
            return result;
        }

        /// <summary>
        /// 新增或修改
        /// </summary>
        [HttpPost]
        public Response AddOrUpdate([FromBody] ReqAuAdminUser req)
        {
            var result = new Response();
            _app.AddOrUpdate(req);
            return result;
        }

        /// <summary>
        /// 批量删除
        /// </summary>
        [HttpPost]
        public Response Delete([FromBody] string[] ids)
        {
            var result = new Response();
            _app.Delete(ids);
            return result;
        }


        /// <summary>
        /// 加载指定角色的用户
        /// </summary>
        [HttpGet]
        public async Task<TableData> LoadByRole([FromQuery] QueryUserListByRoleReq req)
        {
            return await _app.LoadByRole(req);
        }



    }
}
