﻿using Microsoft.AspNetCore.Http;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

using TencentCloud.Cms.V20190321.Models;

namespace Qs.Comm.Middleware
{
    /// <summary>
    /// 演示站,不许操作中间件
    /// </summary>
    public class NoModificationMiddleware
    {
        private readonly RequestDelegate _next;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="next"></param>
        public NoModificationMiddleware(RequestDelegate next)
        {
            _next = next;
        }
        /// <summary>
        /// 执行方法
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public async Task Invoke(HttpContext context)
        {
            var request = context.Request;
            if (context.Request.Method != HttpMethods.Get
                && !context.Request.Path.ToString().ToLower().Contains("check")  //登录/退出/微信绑手机号等
                )
            {
                var platform = context.Request.Headers["platform"].ToString();
                if (platform == Define.AppWebAdmin) //pc管理端
                {
                    context.Response.StatusCode = 200;
                    context.Response.ContentType = "application/json; charset=utf-8";
                    var result = new { code = 500, message = "很抱歉，当前为演示站，不允许任何操作!" };
                    await context.Response.WriteAsync(JsonHelper.Instance.Serialize(result));
                    return;
                }
            }
            await _next(context);
        }
    }
}
